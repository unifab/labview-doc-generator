---
title: About this project
subtitle: AntiDoc is the cure to your LabVIEW project documentation
comments: true
---

This project was born in mid 2019 in order to help LabVIEW developper to improve documentation provided with their projects.


### Goal

Use LabVIEW content to generate valuable documentation that wil help maintaining your projects and onboarding new resources in the development team.

